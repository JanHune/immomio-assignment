package com.immomio.assignment.services.impl;

import com.immomio.assignment.model.ServiceBinding;
import com.immomio.assignment.model.response.OperationResponse;
import com.immomio.assignment.services.BindingService;
import org.springframework.stereotype.Service;

@Service
class BindingServiceImpl implements BindingService {

    @Override
    public OperationResponse createBinding(ServiceBinding serviceBinding, String instanceId, String bindingId) {

        return new OperationResponse("Operation");

    }

    @Override
    public OperationResponse deleteBinding(String instanceId, String bindingId) {

        return new OperationResponse("Operation");

    }
}
