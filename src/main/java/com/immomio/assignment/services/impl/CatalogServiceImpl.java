package com.immomio.assignment.services.impl;

import com.immomio.assignment.model.response.ServicesResponse;
import com.immomio.assignment.services.CatalogService;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
class CatalogServiceImpl implements CatalogService {

    @Override
    public ServicesResponse getService() {

        return new ServicesResponse(Collections.emptyList());
    }
}
