package com.immomio.assignment.services.impl;

import com.immomio.assignment.model.ServiceInstance;
import com.immomio.assignment.model.ServiceInstanceMetadata;
import com.immomio.assignment.model.response.OperationResponse;
import com.immomio.assignment.model.response.ProvisionServiceInstanceResponse;
import com.immomio.assignment.services.InstanceService;
import org.springframework.stereotype.Service;

@Service
class InstanceServiceImpl implements InstanceService {

    @Override
    public ProvisionServiceInstanceResponse provision(String instanceId, ServiceInstance instance) {

        return new ProvisionServiceInstanceResponse("dashBoardUrl", "operation", new ServiceInstanceMetadata());

    }

    @Override
    public OperationResponse delete(String instanceId) {

        return new OperationResponse("Operation");

    }
}
