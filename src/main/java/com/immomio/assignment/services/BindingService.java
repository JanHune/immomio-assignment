package com.immomio.assignment.services;

import com.immomio.assignment.model.ServiceBinding;
import com.immomio.assignment.model.response.OperationResponse;

public interface BindingService {
    OperationResponse createBinding(ServiceBinding serviceBinding, String instanceId, String bindingId);

    OperationResponse deleteBinding(String instanceId, String bindingId);
}
