package com.immomio.assignment.services;

import com.immomio.assignment.model.ServiceInstance;
import com.immomio.assignment.model.response.OperationResponse;
import com.immomio.assignment.model.response.ProvisionServiceInstanceResponse;

public interface InstanceService {

    ProvisionServiceInstanceResponse provision(String instanceId, ServiceInstance instance);

    OperationResponse delete(String instanceId);
}
