package com.immomio.assignment.services;

import com.immomio.assignment.model.response.ServicesResponse;

public interface CatalogService {

    ServicesResponse getService();

}
