package com.immomio.assignment.controller;

import com.immomio.assignment.model.response.ServicesResponse;
import com.immomio.assignment.services.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CatalogController {

    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/v2/catalog")
    public ResponseEntity<ServicesResponse> getCatalog() {

        final var services = catalogService.getService();

        return ResponseEntity.ok(services);

    }

}
