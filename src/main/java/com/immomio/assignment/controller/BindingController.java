package com.immomio.assignment.controller;

import com.immomio.assignment.model.ServiceBinding;
import com.immomio.assignment.model.response.OperationResponse;
import com.immomio.assignment.services.BindingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class BindingController {

    private final BindingService bindingService;

    public BindingController(BindingService bindingService) {
        this.bindingService = bindingService;
    }

    @PutMapping("/v2/service_instances/{instanceId}/service_bindings/{bindingId}")
    public ResponseEntity<OperationResponse> createServiceBinding(@RequestBody ServiceBinding serviceBinding, @PathVariable String instanceId, @PathVariable String bindingId, @RequestParam("accepts_incomplete") Optional<Boolean> acceptsIncomplete) {

        final var operationResponse = bindingService.createBinding(serviceBinding, instanceId, bindingId);

        return ResponseEntity.ok(operationResponse);
    }

    @DeleteMapping("/v2/service_instances/{instanceId}/service_bindings/{bindingId}")
    public ResponseEntity<OperationResponse> deleteServiceBinding(@PathVariable String instanceId, @PathVariable String bindingId, @RequestParam("service_id") String serviceId, @RequestParam("plan_id") String planId, @RequestParam("accepts_incomplete") Optional<Boolean> acceptsIncomplete) {

        final var operationResponse = bindingService.deleteBinding(instanceId, bindingId);

        return ResponseEntity.ok(operationResponse);

    }

}
