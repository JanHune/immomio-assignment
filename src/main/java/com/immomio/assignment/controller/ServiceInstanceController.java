package com.immomio.assignment.controller;

import com.immomio.assignment.model.ServiceInstance;
import com.immomio.assignment.model.response.OperationResponse;
import com.immomio.assignment.model.response.ProvisionServiceInstanceResponse;
import com.immomio.assignment.services.InstanceService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ServiceInstanceController {

    private final InstanceService instanceService;

    public ServiceInstanceController(InstanceService instanceService) {
        this.instanceService = instanceService;
    }

    @PutMapping("/v2/service_instances/{instanceId}")
    public ResponseEntity<ProvisionServiceInstanceResponse> provisionServiceInstance(@PathVariable String instanceId, @RequestBody ServiceInstance instance, @RequestParam("accepts_incomplete") Optional<Boolean> acceptsIncomplete) {

        final var response = instanceService.provision(instanceId, instance);

        return ResponseEntity.ok(response);

    }

    @DeleteMapping("/v2/service_instances/{instanceId}")
    public ResponseEntity<OperationResponse> deleteServiceInstance(@PathVariable String instanceId, @RequestParam("service_id") String serviceId, @RequestParam("plan_id") String planId, @RequestParam("accepts_incomplete") Optional<Boolean> acceptsIncomplete) {

        final var operationResponse = instanceService.delete(instanceId);

        return ResponseEntity.ok(operationResponse);

    }


}
