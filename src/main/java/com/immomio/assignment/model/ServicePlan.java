package com.immomio.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class ServicePlan {

    private final String id;

    private final String name;

    private final String description;

    private Map<String, String> metaData;

    private boolean free;

    private boolean bindable;

    @JsonProperty("plan_updateable")
    private boolean planUpdatable;

    private Schemas schemas;

    @JsonProperty("maximum_polling_duration")
    private int maximumPollingDuration;

    @JsonProperty("maintenance_info")
    private MaintenanceInfo maintenanceInfo;

}
