package com.immomio.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class ServiceInstance {

    @JsonProperty("service_id")
    private final String serviceId;

    @JsonProperty("plan_id")
    private final String planId;

    @JsonProperty("organization_guid")
    private final String organizationGuid;

    @JsonProperty("space_guid")
    private final String spaceGuid;

    private Map<String, String> context;

    private Map<String, String> parameters;

    @JsonProperty("maintenance_info")
    private MaintenanceInfo maintenanceInfo;


}
