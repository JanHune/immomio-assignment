package com.immomio.assignment.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.immomio.assignment.model.ServiceInstanceMetadata;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProvisionServiceInstanceResponse {

    @JsonProperty("dashboard_url")
    private String dashboardUrl;

    private String operation;

    private ServiceInstanceMetadata metadata;


}
