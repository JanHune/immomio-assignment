package com.immomio.assignment.model.response;

import lombok.Data;

@Data
public class OperationResponse {

    private final String operation;

}
