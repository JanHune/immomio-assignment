package com.immomio.assignment.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.immomio.assignment.model.ServiceOffering;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ServicesResponse {

    @JsonProperty("services")
    private List<ServiceOffering> serviceOfferings;

}
