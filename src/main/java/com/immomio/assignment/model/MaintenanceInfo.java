package com.immomio.assignment.model;

import lombok.Data;

@Data
public class MaintenanceInfo {

    private final String version;

    private String description;

}
