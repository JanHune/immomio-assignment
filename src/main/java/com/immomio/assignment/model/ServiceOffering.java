package com.immomio.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class ServiceOffering {

    private final String name;

    private final String id;

    private final String description;

    private final boolean bindable;

    private List<String> tags;

    private List<String> requires;

    @JsonProperty("maintenance_info")
    private boolean instancesRetrievable;

    @JsonProperty("bindings_retrievable")
    private boolean bindingsRetrievable;

    @JsonProperty("allow_context_updates")
    private boolean allowContextUpdates;

    private Map<String, String> metaData;

    @JsonProperty("dashboard_client")
    private DashboardClient dashboardClient;

    @JsonProperty("plan_updateable")
    private boolean planUpdatable;

    private List<ServicePlan> plans;
}
