package com.immomio.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class ServiceBinding {

    @JsonProperty("service_id")
    private final String serviceId;

    @JsonProperty("plan_id")
    private final String planId;

    @JsonProperty("app_guid")
    private String appGuid;

    @JsonProperty("bind_resource")
    private BindResource bindResource;

    private Map<String, String> context;

    private Map<String, String> parameters;

}
