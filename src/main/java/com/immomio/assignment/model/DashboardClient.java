package com.immomio.assignment.model;

import lombok.Data;

@Data
public class DashboardClient {

    private final String id;

    private final String secret;

    private String redirectUri;

}
