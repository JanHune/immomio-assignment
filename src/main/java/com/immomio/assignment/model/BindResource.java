package com.immomio.assignment.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BindResource {

    @JsonProperty("app_guid")
    private String appGuid;

    private String route;

}
